# Header jsonschema validation

This validates the header metadata (from `File-metadata-extractor`) against
the JSON schema provided by the user.

## Examples

*  `empty-json-schema.json`
    * A dummy validation json schema not validating anything, but acts as the default schema.
* `basic-only-dicom-dicom-array.json`:
  * This schema only checks to make sure that the passed in header has the keys
  `dicom` and `dicom_array`
* `deid-validation.json`
  * This schema showcases how to make sure certain DICOM tags are NOT present in the
  header.
  * It uses the JSON schema's `"not"` key used to negate its value, followed by
  `"anyOf"`. This `"anyOf"` will evaluate to `true` if any of the required fields that
  follow is present. With the `"not"` in front of it, it will check that none of
  `PatientName`, `PatientAddress`, `ReferringPhysicianName` and `RequestingPhysician`
  is present in the header.
* `extended-dicom-dicom-array-validation.json`
  * This schema does more extensive validation, it makes sure that both `dicom`
  and `dicom_array` are present, but it also ensures specific keys are present on each:
  * `dicom`:
    * Key `Modality`, if present, must be a string, and must be one of `CT`, `PT`,
    `MR`.
    * Key `ImageType`, if present, must be an array, must not contain the value
    `LOCALIZER`, and must contain one of `PRIMARY`.`AXIAL`, or `ORIGINAL`
    * The keys `AcquisitionDate`, `Columns`, `ConvolutionKernel`,
    `ImageOrientationPatient`, and `ImagePositionPatient` must be present.
  * `dicom_array`: Must contain the key `InstanceNumber`
