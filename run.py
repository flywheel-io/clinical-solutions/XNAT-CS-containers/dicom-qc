#!/usr/bin/env python
"""The run script."""
import logging
import sys
import os
import json
import traceback
from fw_gear_dicom_qc import main, parser, utils
from fw_gear_dicom_qc.xnat_logging import stderr_log,stdout_log

def entry() -> None: 
    """Parses config and run."""
    try:
        dcm_path,dcm_header_path, schema, rules = parser.parse_config()
        validation_results,validation_ran, rule_results = main.run(dcm_path,dcm_header_path,schema, rules)

        qc = {"file": {"info": {"qc": {"dicom-qc":{}}}}}

        if len(validation_results)==0 and validation_ran:
            header_validation={"jsonschema-validation":{"state":"PASS","data":validation_results}}
        else:
            header_validation={"jsonschema-validation":{"state":"FAIL","data":validation_results}}

        for result in [header_validation,rule_results]:
            if isinstance(result,dict):
                qc["file"]["info"]["qc"]["dicom-qc"].update(result)

        with open("/output/dicom_qc.json", "w") as dicomqc:
            json.dump(qc, dicomqc)

        if os.path.exists("/output/dicom_qc.json"):
            stdout_log.info("Saved /output/dicom_qc.json")
   
    except Exception as exp: 
        traceback_info = traceback.format_exc()
        stdout_log.error(f"An error occurred: %s. Check the stderr.log for traceback.",exp)

        stderr_log.error("%s %s",exp,traceback_info)
        sys.exit(1) # give a Failed status

if __name__ == "__main__":  # pragma: no cover
    entry()
