FROM amd64/gcc:11.2.0-bullseye as build

COPY dicom3tools.tar.gz /build/
WORKDIR /build

RUN apt-get update && \
    apt-get install -y --no-install-recommends "xutils-dev=1:7.7+5+b1" && \
    tar -xzvf dicom3tools.tar.gz && \
    cd dicom3tools && \
    ./Configure && \
    imake -I./config -DInstallInTopDir && \
    make World && \
    make install && \
    mkdir /root/dicom3tools && \
    find bin -type f -exec mv '{}' /root/dicom3tools \; && \
    rm -rf /var/lib/apt/lists/*

FROM python:3.10.14-slim as gear
ENV FLYWHEEL="/flywheel/v0"

WORKDIR ${FLYWHEEL}

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

COPY ./ $FLYWHEEL/
COPY --from=build /root/dicom3tools/ /usr/local/bin/

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["python","/flywheel/v0/run.py"]

LABEL org.nrg.commands="[{\"name\": \"dicom-qc\", \"label\": \"dicom-qc\", \"description\": \"Runs dicom-qc v1.1\", \"version\": \"1.1\", \"schema-version\": \"1.0\", \"image\": \"registry.gitlab.com/flywheel-io/clinical-solutions/xnat-cs-containers/dicom-qc:1.1\", \"type\": \"docker\", \"command-line\": \"python run.py --debug  #DEBUG# --project #PROJECT# --validation_schema #SCHEMA# --check_bed_moving #BED# --check_dciodvfy #DCIODVFY# --check_embedded_localizer #LOCALIZER# --check_instance_number_uniqueness #INSTUNIQ# --check_series_consistency #SERIES# --check_slice_consistency #SLICE#\", \"override-entrypoint\": true, \"mounts\": [{\"name\": \"in\", \"writable\": false, \"path\": \"/input\"}, {\"name\": \"out\", \"writable\": true, \"path\": \"/output\"}], \"environment-variables\": {}, \"ports\": {}, \"inputs\": [{\"name\": \"debug\", \"description\": \"Include debug statements in the output\", \"type\": \"select-one\", \"default-value\": \"False\", \"required\": false, \"replacement-key\": \"#DEBUG#\", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"validation_schema\", \"description\": \"jsonschema template for validating header\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#SCHEMA#\", \"default-value\": \"empty-json-schema.json\"}, {\"name\": \"check_bed_moving\", \"description\": \"Run check_bed_moving rule\", \"type\": \"select-one\", \"default-value\": \"True\", \"required\": false, \"replacement-key\": \"#BED#\", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"check_dciodvfy\", \"description\": \"Run check_dciodvfy rule\", \"type\": \"select-one\", \"default-value\": \"True\", \"required\": false, \"replacement-key\": \"#DCIODVFY#\", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"check_embedded_localizer\", \"description\": \"Run check_embedded_localizer rule\", \"type\": \"select-one\", \"default-value\": \"True\", \"required\": false, \"replacement-key\": \"#LOCALIZER#\", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"check_instance_number_uniqueness\", \"description\": \"Run check_instance_number_uniqueness rule\", \"type\": \"select-one\", \"default-value\": \"True\", \"required\": false, \"replacement-key\": \"#INSTUNIQ#\", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"check_series_consistency\", \"description\": \"Run check_series_consistency rule\", \"type\": \"select-one\", \"default-value\": \"True\", \"required\": false, \"replacement-key\": \"#SERIES#\", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"check_slice_consistency\", \"description\": \"Run check_slice_consistency rule\", \"type\": \"select-one\", \"default-value\": \"True\", \"required\": false, \"replacement-key\": \"#SLICE#\", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"project\", \"description\": \"project id\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#PROJECT#\", \"select-values\": []}], \"outputs\": [{\"name\": \"dicom-qc\", \"description\": \"dicom-qc\", \"required\": true, \"mount\": \"out\", \"glob\": \"*.json\"}], \"xnat\": [{\"name\": \"dicom-qc\", \"label\": \"dicom-qc-v1.1\", \"description\": \"Run dicom-qc v1.1\", \"contexts\": [\"xnat:imageScanData\"], \"external-inputs\": [{\"name\": \"scan\", \"description\": \"Input scan\", \"type\": \"Scan\", \"required\": true, \"provides-files-for-command-mount\": \"in\", \"load-children\": false}], \"derived-inputs\": [{\"name\": \"session\", \"type\": \"Session\", \"required\": true, \"user-settable\": false, \"load-children\": true, \"derived-from-wrapper-input\": \"scan\", \"multiple\": false}, {\"name\": \"project\", \"type\": \"string\", \"required\": true, \"provides-value-for-command-input\": \"project\", \"load-children\": true, \"derived-from-wrapper-input\": \"session\", \"derived-from-xnat-object-property\": \"project-id\", \"multiple\": false}], \"output-handlers\": [{\"name\": \"dicom-qc\", \"accepts-command-output\": \"dicom-qc\", \"as-a-child-of\": \"scan\", \"type\": \"Resource\", \"label\": \"dicom-qc\", \"tags\": []}]}], \"reserve-memory\": 500, \"container-labels\": {}, \"generic-resources\": {}, \"ulimits\": {}, \"secrets\": []}]"