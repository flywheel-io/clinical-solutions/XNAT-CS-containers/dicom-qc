"""Main module."""
import logging
import shutil
import sys
import tempfile
import typing as t
import zipfile
import json
import os
from fw_file.dicom.series import DICOMCollection
from fw_utils.files import fileglob
from . import rules, validation
from .xnat_logging import stdout_log,stderr_log

default_rules = [
    "check_series_consistency",
    "check_instance_number_uniqueness",
    "check_embedded_localizer",
    "check_bed_moving",
    "check_slice_consistency",
    "check_dciodvfy",
]

def eval_rules(dcms: DICOMCollection, rule_dict: t.Dict[str, bool]) -> t.List[rules.RuleReport]:
    """Evaluate qc rules on the given file.

    Args:
        file_path (AnyPath): Path to file
        rule_dict (t.Dict[str, bool]): Dictionary of rules and whether or not
            to run them.

    Returns:
        t.List[rules.RuleReport]: Results of evalution of each rule.
    """
    # Evaluate all rules and keep list of reports.
    reports: t.List[rules.RuleReport] = []
    rules_list = [rule for rule, val in rule_dict.items()]

    for rule in rules_list:
        rule_fn = getattr(rules, rule)
        result = rule_fn(dcms)
        reports.append(result)

    return reports


def run(dicom_path:str, dcm_header_path:str, schema: t.Dict, rule_dict: t.Dict):
    """Run dicom-qc entrypoint.
        Args:
        dicom_path (str): str path to dicom directory
        dcm_header_path (str): str path to header file from file-metadata-extractor
        schema (t.Dict): the validation schema read in a dictionary
        rule_dict (t.Dict): Dictionary of rules

    Returns:
        validation_results (List): results from header validation
        validation_ran (Bool): if header validation ran
        formatted_results (Dict): rules from QC rules
    """
    validation_ran=False
    validation_results=[]
    # if /input/file-metadata-extractor/DICOM_header.json is found then try to validate header with schema
    if not dcm_header_path is None:
        stdout_log.info("Checking format of provided schema")
        if not validation.validate_schema(schema):
            sys.exit(1)

        stdout_log.info("Validating file.info.header with validation schema")

        with open(dcm_header_path, "r") as fp:
            dicom_header = json.load(fp)
        validation_results = validation.validate_header(dicom_header["file"]["info"]["header"], schema)
        validation_ran=True

        if len(validation_results) ==0 and validation_ran:
            stdout_log.info("jsonschema-validation PASSED")
        
        if len(validation_results) > 0:
            stdout_log.info("jsonschema-validation FAILED")
        
        validation_results = [result.__dict__ for result in validation_results]

    if not validation_ran:
        stdout_log.info("jsonschema-validation FAILED")
        validation_results=["file-metadata-extractor not found"]

    stdout_log.info("Evaluating qc rules:")

    # dicom_path has .xml file, and could have either .IMA or .dcm file extension.
    dicoms_fileglob=fileglob(dicom_path, recurse=True,pattern="*.dcm") + fileglob(dicom_path, recurse=True,pattern="*.IMA")

    # dicom file extension. (this should either be .dcm or .IMA)
    dcm_file_ext= "*" + dicoms_fileglob[0].suffix
    check_0_report = rules.check_0_byte(dicoms_fileglob)

    if check_0_report.state != "PASS":
        stdout_log.error("Single dicom file 0-byte")
        sys.exit(1)
    # get all dicom files into the dicom collection object
    dcms = DICOMCollection.from_dir(dicom_path, stop_when=None, force=True,pattern=dcm_file_ext)    
    
    # evaluate enabled rules
    rule_results = eval_rules(dcms, rule_dict)
    rule_results.append(check_0_report)

    formatted_results = {}
    for result in rule_results:
        val = result.__dict__
        rule = val.pop("rule")
        formatted_results[rule] = val

    return validation_results,validation_ran, formatted_results
