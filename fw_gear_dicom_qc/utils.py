"""Utilities module for fw_gear_dicom_qc."""
import xnat
import os
from .xnat_logging import stdout_log

def get_validation_schema(project:str,validation_schema:str)->str:
    """ Get JSON validation schema from project resources
    project : str
        project id
    validation_schema : str
        validation schema name to search for in project resources

    Returns:
        path to validation schema: str
    """
    work_dir="/flywheel/v0"

    host=os.environ["XNAT_HOST"]
    user=os.environ["XNAT_USER"]
    password=os.environ["XNAT_PASS"]

    session = xnat.connect(host, user=user, password=password)
    proj_resources=session.get_json(f"/data/projects/{project}/files")

    filenames = [validation_schema]
   
    # look for the specified files in project resource files
    for result in proj_resources['ResultSet']['Result']:
        if result['Name'] in filenames:
            file_uri=result['URI']
            if file_uri:
                session.download(file_uri, f"/{work_dir}/{result['Name']}")
                session.disconnect()

                if os.path.exists(f"/{work_dir}/{result['Name']}"):
                    stdout_log.info(f"Found and copied {result['Name']} to {work_dir}")
                    return f"/{work_dir}/{result['Name']}"
                else:
                    stdout_log.error("Unable to copy {result['Name']} to {work_dir}")
                    return None

                    
    

     