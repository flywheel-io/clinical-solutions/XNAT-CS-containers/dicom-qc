"""Parser module to parse the config"""
import argparse
import json
import os
import sys
import typing as t
import logging
from pathlib import Path
from .utils import get_validation_schema
from .xnat_logging import stderr_log,stdout_log
from . import validation
def parse_config() -> t.Tuple[str, str, t.Dict,t.Dict]:
    """Parse config returns relevant inputs and config.

    Args:
        None:None

    Returns:
        t.Tuple[t.Str, t.Str, t.Dict,t.Dict]:
            - String path to where DICOMs are stored
            - String path to DICOM header.json
            - Loaded schema
            - Rules dictionary in the format <rule>:<bool>
    """

    parser = argparse.ArgumentParser(description='DICOM-QC')
    parser.add_argument('--project',required=True)
    parser.add_argument('--debug',required=False,default="False")

    parser.add_argument('--validation_schema',required=True,default="empty-json-schema.json")
    
    parser.add_argument('--check_bed_moving',required=False,default="True")
    parser.add_argument('--check_dciodvfy',required=False,default="True")
    parser.add_argument('--check_embedded_localizer',required=False,default="True")
    parser.add_argument('--check_instance_number_uniqueness',required=False,default="True")
    parser.add_argument('--check_series_consistency',required=False,default="True")
    parser.add_argument('--check_slice_consistency',required=False,default="True")

    args=parser.parse_args() 

    if args.debug =="False":
            stdout_log.info("Disabling debug level logs")
            stdout_log.setLevel(logging.INFO)

    # confirm that /input/DICOM exists and that it contains either .dcm or .IMA files 
    dcm_path='/input/DICOM'
    if os.path.exists(dcm_path):
        dcm_files = [file for file in os.listdir(dcm_path) if file.endswith(".dcm") or file.endswith(".IMA")]

        if len(dcm_files) ==0:
            stderr_log.error("No DICOM files found with .dcm or .IMA file extension")
            sys.exit(1)
        dcm_files=[]
    else:
        stderr_log.info("DICOM directory does not exist.")
        sys.exit(1)

    # Need file-metadata-extractor for validation-schema
    if not os.path.exists("/input/file-metadata-extractor/DICOM_header.json"):
         stdout_log.warning("file-metadata-extractor/DICOM_header.json not found under the scan resource." 
            " This is required for the validation-schema check. Skipping jsonschema-validation QC check will be set to FAIL. ")
         dcm_header_path=None
         schema={}
    else:
         dcm_header_path="/input/file-metadata-extractor/DICOM_header.json"
         schema_path="/flywheel/v0/examples/empty-json-schema.json"
         
          # get provided validation-schema json from project resource
         if args.validation_schema != "empty-json-schema.json":
          schema_path=get_validation_schema(args.project,args.validation_schema)

          # if there are any issues, then default to the empty json schema
         if schema_path is None:
            stdout_log.error(f"Could not find{args.validation_schema} in project resources. Using default empty-json-schema.json instead. ")
            schema_path="/flywheel/v0/examples/empty-json-schema.json"

         with open(schema_path, "r") as fp:
               schema = json.load(fp)

         if not validation.validate_schema(schema):
               sys.exit(1)

    # populate rules dict with enabled QC rules
    rules={}
    if args.check_bed_moving == "True":
         rules["check_bed_moving"]={}

    if args.check_dciodvfy == "True":
         rules["check_dciodvfy"]={}

    if args.check_embedded_localizer == "True":
         rules["check_embedded_localizer"]={}

    if args.check_instance_number_uniqueness == "True":
         rules["check_instance_number_uniqueness"]={}

    if args.check_series_consistency == "True":
         rules["check_series_consistency"]={}

    if args.check_slice_consistency == "True":
         rules["check_slice_consistency"]={}

    return dcm_path,dcm_header_path, schema, rules
