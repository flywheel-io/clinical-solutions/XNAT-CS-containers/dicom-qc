# Release notes

1.1
---
- Corresponds to DICOM QC Gear v0.4.13
- **Enhancements**:
    - Improved dciodvfy output logs
    - Restructuring of jsonschema-validation error messages due to updated jsonschema package

- Updated Splitter dependancy
- Updated fw-file
  - Improved handling of invalid CS values
  - Improved handling of invalid DS/IS values
- Changed output QC namespace format

1.0 
---
- Initial release corresponding to DICOM QC Gear v0.4.9