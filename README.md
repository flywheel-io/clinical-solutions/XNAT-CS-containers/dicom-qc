# DICOM QC

This scan-level container evaluates [QC Rules](#qc-rules) against DICOMs in a scan. This container corresponds to the `0.4.13` tag of the [DICOM QC](https://gitlab.com/flywheel-io/flywheel-apps/dicom-qc/-/tree/0.4.13?ref_type=tags) gear.

### Release notes

**1.1**
- Corresponds to DICOM QC Gear v0.4.13
- **Enhancements**:
    - Improved dciodvfy output logs
    - Restructuring of jsonschema-validation error messages due to updated jsonschema package
- Updated Splitter dependancy
- Updated fw-file
  - Improved handling of invalid CS values
  - Improved handling of invalid DS/IS values
- Changed output QC namespace format

**1.0**
- Initial release corresponding to DICOM QC Gear v0.4.9
---

- [DICOM QC](#dicom-qc)
    - [Release notes](#release-notes)
  - [Summary](#summary)
  - [Configs](#configs)
  - [QC Rules](#qc-rules)
      - [check\_0\_byte](#check_0_byte)
      - [jsonschema\_validation](#jsonschema_validation)
      - [check\_bed\_moving](#check_bed_moving)
      - [check\_dciodvfy](#check_dciodvfy)
      - [check\_embedded\_localizer](#check_embedded_localizer)
      - [check\_instance\_number\_uniqueness](#check_instance_number_uniqueness)
      - [check\_series\_consistency](#check_series_consistency)
      - [check\_slice\_consistency](#check_slice_consistency)
  - [Output](#output)

## Summary
- It's assumed that DICOM files (with extension .dcm or .IMA) are saved in a folder called DICOM under the scan.
- `DICOM QC` can validate the header against custom rules (see [jsonschema\_validation](#jsonschema_validation)). In order to do this [file-metadata-extractor](https://gitlab.com/flywheel-io/clinical-solutions/XNAT-CS-containers/file-metadata-extractor) container must be run. `DICOM QC` will look for **file-metadata-extractor/DICOM_header.json**
  - <img src="./images/prereq.png" width=35% />
  - Additionally, the user provided JSON schema (**validation-schema** input) must exist as a file in the project's resource. It's recommended to have this file stored in a resource folder called *dicom-qc*. XNATpy is used to pull this file into the container.
    - <img src="./images/project_resource.png" width=35% />
  - See [examples/README.md](./examples/README.md) for example validation schemas
- If the container is rerun on a scan, any pre-existing [outputs](#output) will be overwritten
- Performance
  - `reserve-memory` of 500 MB was set in the `command.json`.
  - Only 1 CPU is required
  
## Configs
- *debug*
  - __Description__: *Include debug statements in output.*
  - __Default__: `False`
- *validation-schema*
  - __Description__: *jsonschema template for validating header.*
  - __Default__: `empty-json-schema.json`
- *check_bed_moving*
  - __Description__: *Run check_bed_moving rule.*
  - __Default__: `True`
- *check_dciodvfy*
  - __Description__: *Run the `dciodvfy` (DICOM IOD Verify) binary .*
  - __Default__: `True`
- *check_embedded_localizer*
  - __Description__: *Run check_embedded_localizer rule.*
  - __Default__: `True`
- *check_instance_number_uniqueness*
  - __Description__: *Run check_instance_number_uniqueness rule.*
  - __Default__: `True`
- *check_series_consistency*
  - __Description__: *Run check_series_consistency rule.*
  - __Default__: `True`
- *check_slice_consistency*
  - __Description__: *Run check_slice_consistency rule.*
  - __Default__: `True`



## QC Rules

#### check_0_byte
- This rule is run by default and checks each file in the scan to make sure it is not 0-bytes.
#### jsonschema_validation
- This rule validates the DICOM header (from file-metadata-extractor) to the user provided JSON schema, which acts as the ground truth.
- See [examples/README.md](./examples/README.md) for example JSON schemas.
#### check_bed_moving
- This rule evaluates whether the bed was moving during the entirety of a scan
(determined by `ImagePositionPatient`).
#### check_dciodvfy
- This rule runs the `dciodvfy` (DICOM IOD Verify). More info can be found [here](https://www.dclunie.com/dicom3tools/dciodvfy.html).
#### check_embedded_localizer
- This rule checks the scan for an embedded localizer frame using an algorithm from the 
[splitter](https://gitlab.com/flywheel-io/flywheel-apps/splitter) gear.
#### check_instance_number_uniqueness
- This rule checks to make sure there aren't any duplicate `InstanceNumber` values in the scan.

#### check_series_consistency
- This rule checks to make sure there is only one series in the scan
(determined by `SeriesInstanceUID`)

#### check_slice_consistency
- This rule checks whether the intervals between slice positions are consistent.
If the `SliceLocation` tag is present on all files in the scan, this will be used.
If not, the slice location will be calculated from `ImagePositionPatient` and
`ImageOrientationPatient`.



## Output
`DICOM QC` will store the QC results in **dicom_qc.json**  under the **dicom-qc**  scan resource
  - <img src="./images/output_filestruc.png" width=35% />
  - QC rules that were not run (disabled during container launch) will not appear in **dicom_qc.json**
  - The sample output below contains the `file.info.qc.dicom-qc` namespace which has QC results for each rule run:
    - **bed_moving**, **embedded_localizer**, **instance_number_uniqueness**, **series_consistency**, **slice_consistency**, **dciodvfy**, and  **check_zero_byte** keys all follow this format:
      ```json
      "rule-name":{
          "state": This is either PASS or FAIL,
          "data": If the "state" was FAIL, then this field is populated with what was wrong. Otherwise it will be set to: null.

      }
      ```
    - **jsonschema-validation** key follows this format:
      ```json
        "jsonschema-validation":{
              "state": This is either PASS or FAIL,
              "data": If the "state" was FAIL, then this field is populated with the output from the jsonschema validator.
          }
      ```


**dicom_qc.json**:
  ```json
    {
      "file": {
        "info": {
          "qc": {
            "dicom-qc": {
              "jsonschema-validation": {
                "state": "FAIL",
                "data": [
                  {
                    "error_type": "not",
                    "error_message": "{'SeriesDescription': 'T1w MPRage' 'PatientName': 'First Last' ETC.. This has been truncated for this readme. However in the place of this message you will see the contents of DICOM_header.json here} should not be valid under {'anyOf': [{'required': ['PatientName']} {'required': ['PatientAddress']} {'required': ['ReferringPhysicianName']} {'required': ['RequestingPhysician']}]} ",
                    "error_value": {
                      "anyOf": [
                        { "required": ["PatientName"] },
                        { "required": ["PatientAddress"] },
                        { "required": ["ReferringPhysicianName"] },
                        { "required": ["RequestingPhysician"] }
                      ]
                    },
                    "error_context": "",
                    "item": "file.info.header.dicom"
                  }
                ]
              },
              "bed_moving": { "state": "PASS", "description": null },
              "dciodvfy": {
                "state": "FAIL",
                "data": [
                            {
                                "name": "Error - </PatientBirthDate(0010,0030)> - Missing attribute for Type 2 Required - Module=<Patient>",
                                "slices": "all"
                            },
                            {
                                "name": "Error - </AccessionNumber(0008,0050)> - Missing attribute for Type 2 Required - Module=<GeneralStudy>",
                                "slices": [1,2,30,40]
                            }
                ]
              },
              "embedded_localizer": {
                "state": "FAIL",
                "data": "Found localizer within archive."
              },
              "instance_number_uniqueness": {
                "state": "FAIL",
                "data": "Found 53 InstanceNumbers across 55 frames."
              },
              "series_consistency": {
                "state": "FAIL",
                "data": "2 unique SeriesInstanceUIDs found"
              },
              "slice_consistency": {
                "state": "FAIL",
                "data": "Inconsistent slice intervals.  Majority are ~5.0000mm(52), but also found \n0.0000, 38.2500"
              },
              "check_zero_byte": { "state": "PASS", "data": null }
            }
          }
        }
      }
    }